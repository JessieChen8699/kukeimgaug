import cv2
import numpy as np
import math
import random
from pathlib import Path


class Glasses_augmenter():
    def __init__(self,mode,type_idx=1):
        self.mode = mode
        self.glasses = self.Get_glasses()
        self.type_idx = type_idx
        
    def Get_glasses(self):
        glasses_root = f'material/glasses/processed_glasses/{self.mode}'
        total_glasses = [cv2.imread(str(path),cv2.IMREAD_UNCHANGED) for path in Path(glasses_root).rglob('*.png')]
        print(f'Total types of glasses:{len(total_glasses)}')
        return total_glasses
        
    def RGB2RGBA(self,rgb_img,threshold):
        gray_img = cv2.cvtColor(rgb_img, cv2.COLOR_BGR2GRAY)
        _, alpha = cv2.threshold(gray_img,threshold,255,cv2.THRESH_BINARY)
        b, g, r = cv2.split(rgb_img)
        rgba_img = cv2.merge([b,g,r, alpha],4)
        return rgba_img


    def TransparentOverlay(self,background, foreground, position_x = 0, position_y = 0, scale=1):
        foreground = cv2.resize(foreground,(0,0),fx=scale,fy=scale)
        h,w,_ = foreground.shape 
        rows,cols,_ = background.shape  
        x,y = position_x, position_y

        for i in range(h):
            for j in range(w):
                if y+i >= rows or x+j >= cols:
                    continue
                alpha = float(foreground[i][j][3]/255.0) 
                background[y+i][x+j][:3] = alpha*foreground[i][j][:3] + (1-alpha)*background[y+i][x+j][:3]
        return background

    def Get_glasses_crop_coodinate(self,angle):
        if abs(angle)<=5:
                start,end = 20,100
        elif angle <- 5:
                start,end = 20,120
        elif angle > 5:
                start,end = 0,100
        return start,end 
    
    
    def run(self,img,yaw):
        img = self.RGB2RGBA(img,-1)
        
        paste_glasses  = self.glasses[self.type_idx]
        start,end = self.Get_glasses_crop_coodinate(yaw)
        paste_glasses = paste_glasses[:,start:end,:]
        augmented_img = self.TransparentOverlay(
                                 img,
                                 paste_glasses,
                                 position_x = random.randint(13,19),
                                 position_y = random.randint(40,44),
                                 scale = 1)
        return augmented_img