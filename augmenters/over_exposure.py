import cv2
import numpy as np
import math
import random
from pathlib import Path


class OverExposure_augmenter():
    def __init__(self, width=(5,10), high=(5,10), color=(250,255), position=[[38, 51],[73, 51]]):
        self.width_range = width
        self.high_range = high
        self.color_range = color
        self.position = position
        
    def Get_random_value(self,range_value):
        return np.random.choice(list(range(range_value[0],range_value[1]+1)))

    def Draw_op(self,img, op_width, op_high, op_color, op_position):
        offset_width = int(op_width/2)
        offset_high = int(op_high/2)
        for x_idx in range(0,len(op_position)):
            op_position_x,op_position_y = op_position[x_idx]
            img[op_position_y-offset_high:op_position_y+offset_high, op_position_x-offset_width:op_position_x+offset_width] = op_color
        return img
    
    def run(self,img):
        op_width = self.Get_random_value(self.width_range)
        op_high = self.Get_random_value(self.high_range)
        op_color = self.Get_random_value(self.color_range)
        augmented_img = self.Draw_op(img, op_width, op_high, op_color, self.position)
        return augmented_img