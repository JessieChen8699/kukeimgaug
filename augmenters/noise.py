import cv2
import numpy as np
import math
import random
from pathlib import Path
import imgaug.augmenters as iaa


class Noise_augmenter():
    def __init__(self, distance=100):
        self.distance = distance # 100 120 

    def add_gausian_noise(self,img,noise_level1):
        mean = 0.0
        std = noise_level1    
        noisy_img = img + np.random.normal(mean, std, img.shape)
        noisy_img_clipped = np.clip(noisy_img, 0, 255) 
        return noisy_img_clipped

    def kmean(self,img,cluster_num):
        data = img.reshape((-1, 1))
        data = np.float32(data)
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        flags = cv2.KMEANS_PP_CENTERS
        compactness, labels, centers = cv2.kmeans(data, cluster_num, None, criteria, 10, flags)
        centers64 = np.uint8(centers)
        res = centers64[labels.flatten()]
        dst = res.reshape((img.shape))
        return dst
    
    def augment_100cm(self,img):
        contrast = iaa.Alpha(0.4, iaa.HistogramEqualization())
        re_size = (int(112*(1/1.16)),int(112*(1/1.16)))
        uniform_size = int(112*(1/2.3499999999999925) * int(112*(1/2.3499999999999925)))
        uniform = iaa.UniformVoronoi(uniform_size, p_replace=0.8, max_size=None) 
        aug_img = cv2.resize(img,re_size,interpolation=cv2.INTER_LINEAR)
        aug_img = uniform.augment_image(aug_img)

        aug_img = self.kmean(aug_img,cluster_num=15)
        aug_img = self.add_gausian_noise(aug_img,7.929999999999997)
        aug_img = np.array(aug_img,dtype='uint8')
        aug_img = contrast.augment_image(aug_img)

        aug_img = cv2.resize(aug_img,(112,112),interpolation=cv2.INTER_LINEAR)
        aug_img = np.array(aug_img,dtype='uint8')
        aug_img = cv2.cvtColor(aug_img,cv2.COLOR_BGR2GRAY)
        aug_img = self.kmean(aug_img,cluster_num=17)
        return aug_img
    def augment_120cm(self,img):
        contrast = iaa.Alpha(0.4, iaa.HistogramEqualization())
        re_size = (int(112*(1/1.27)),int(112*(1/1.27)))
        uniform_size = int(112*(1/4.999999999999998) * int(112*(1/4.999999999999998)))
        uniform = iaa.UniformVoronoi(uniform_size, p_replace=0.8, max_size=None) 
        aug_img = cv2.resize(img,re_size,interpolation=cv2.INTER_LINEAR)
        aug_img = uniform.augment_image(aug_img)

        aug_img = self.kmean(aug_img,cluster_num=15)
        aug_img = self.add_gausian_noise(aug_img,7.929999999999997)
        aug_img = np.array(aug_img,dtype='uint8')
        aug_img = contrast.augment_image(aug_img)

        aug_img = cv2.resize(aug_img,(112,112),interpolation=cv2.INTER_LINEAR)
        aug_img = np.array(aug_img,dtype='uint8')
        aug_img = cv2.cvtColor(aug_img,cv2.COLOR_BGR2GRAY)
        aug_img = self.kmean(aug_img,cluster_num=17)
        return aug_img
    
    
    def run(self,img):
        if self.distance == 100:
            augmented_img = self.augment_100cm(img)
        elif self.distance == 120:
            augmented_img = self.augment_120cm(img)
        
        return augmented_img