import matplotlib.pyplot as plt
import cv2

class Sequential():
    def __init__(self,modules):
        self.modules = modules
        
    def run(self, img_row, show_progress=False):
        
        self.img = cv2.imread(img_row['path'])
        
        if self.img is not None:
            for i in range(len(self.modules)):
                module_name = str(self.modules[i]).split('.')[1]

                if module_name == 'glasses':
                    self.img = self.modules[i].run(self.img,yaw=img_row['yaw'])
                else:
                    self.img = self.modules[i].run(self.img)
                if show_progress:
                    print(module_name)
                    plt.imshow(self.img,cmap='gray')
                    plt.show()
            return self.img
        else:
            print(f'NoneTypeError:{img_row["path"]}')