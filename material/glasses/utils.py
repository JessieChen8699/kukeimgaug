import os
import cv2
from pathlib import Path
import numpy as np


def read_img(path):
    bg = cv2.imread(path,cv2.IMREAD_UNCHANGED)
    h, w ,c= bg.shape
    new_w = 80
    new_h = (h*new_w)/w
    glasses = cv2.resize(bg,(int(new_w), int(new_h)))
    
    temp = np.zeros([glasses.shape[0],120,4],dtype=np.uint8)
    temp[:,:,:2].fill(255)
    temp[:,20:100,:] = glasses
    return temp

def get_unfinished_glasses_path(root):
    glasses_paths = sorted([str(path) for path in Path(root).rglob('*.png') if 'ipy'not in str(path) and os.path.isfile(str(path).replace('orignal_rgba','processed'))==False])
    print('Number of unfinished glasses:',len(glasses_paths))
    return glasses_paths

def get_foot_patch(root):
    return cv2.imread(root.replace('orignal_rgba_glasses','foot')+'.png',cv2.IMREAD_UNCHANGED)