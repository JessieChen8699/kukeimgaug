# KukeImgAug API

## Inroduction

This API is used for augmenting images.

## Installation



## Augmenter

| Augmenter                          | Parameters                                                                          |
| :-----                             | :----                                                                               |
| `Glasses_augmenter`                | `mode='IR'`,`type_idx=0`                                                           |
| `OverExposure_augmenter`           | `width=(5,10)`, `high=(5,10)`, `color=(250,255)`, `position=[[38, 51],[73, 51]]`   |
| `Noise_augmenter`                  | `distance=100 `                                                                     |
| `HeadPose_augmenter`               | `pitch=(-15,15)` ,`roll=(-15,15)`, `yaw=(-15,15)`                                   |

## Parameter

| Glasses_augmenter      | Type       | Example             | Description                                                       |
| :-----                 | :----      |:----                |:----                                                              |
| `mode`                 | `str`      | 'IR','RGB'          | Mode for pasted glasses.                                          |
| `type_idx`             | `int`      | 0,1,2,3,4,5         | Index of differnt types of glasses.                               |

| OverExposure_augmenter | Type       | Example             | Description                                                       |
| :-----                 | :----      |:----                |:----                                                              |
| `width`                | `tuple`    | (5,10)              | Width's range of pasted over-exposured blocks. `(min,max)`        |
| `high`                 | `tuple`    | (5,10)              | High's range of pasted over-exposured blocks.  `(min,max)`        |
| `color`                | `color`    | (250,255)           | Color's range of pasted over-exposured blocks. `(min,max)`        |
| `position`             | `list`     | [[38, 51],[73, 51]] | Position's center of pasted over-exposured blocks.`[p1,p2,p3,...]`|

| Noise_augmenter        | Type       | Example             | Description                                                       |
| :-----                 | :----      |:----                |:----                                                              |
| `distance`             | `int`      | 100,120             | Distance of augmented image. `100` or `120`                       |

| HeadPose_augmenter     | Type       | Example             | Description                                                       |
| :-----                 | :----      |:----                |:----                                                              |
| `pitch`                | `tuple`    | (-15,15)            | Pitch's range of augmented image.                                 |
| `yaw`                  | `tuple`    | (-15,15)            | Yaw's range of augmented image.                                   |
| `roll`                 | `tuple`    | (-15,15)            | Roll's range of augmented image.                                  |



## Example

```python
import pandas as pd
from augmenters.glasses import *
from augmenters.over_exposure import *
from augmenters.noise import *
from sequential import *

atttibute_dataframe = pd.read_csv('example/atttibute.csv')

sequence = Sequential([
    Glasses_augmenter(mode='IR',type_idx=0),
    OverExposure_augmenter(width=(5,10), high=(5,10), color=(250,255), position=[[38, 51],[73, 51]]),
    Noise_augmenter(distance=100),
    ])

img_row = atttibute_dataframe.iloc[0]
augmented_imgs = sequence.run(img_row,show_progress=True)
```
